import logging
import re

from utils.Weather import Weather

logger = logging.getLogger("HoeWarmIsHetInDelft")


class WeatherPropertiesParser:
    @staticmethod
    def parse(data: str, weather_object: Weather):
        """Parses data from the weerindelft.nl server

        Args:
            data (str): data from the weerindelft.nl server
            weather_object (Weather): Weather object that will be populated with parsed data

        Raises:
            value_error: raised if current temperature is not castable to float
            index_error: raised if unable to reach current temperature value in the data list
            ValueError: raised if received data from the server is not matching the checking pattern
        """
        # Valid server response should start with "12345", and end with "!!"
        if re.match(r"^12345[\w\W]+\!\!\s?$", data):
            data_chunks = data.split()
            try:
                # Current temperature is a 4'th element of the data string
                weather_object.current_temperature = data_chunks[4]
            except ValueError as value_error:
                logger.warning(f"Failed to convert {data_chunks[4]} into float")
                raise value_error
            except IndexError as index_error:
                logger.error(
                    f"Failed to fetch current temperature from the data list: {data_chunks}"
                )
                raise index_error
        else:
            raise ValueError(
                f"Expected 12345 at the beginning and !! at the end of a data, actual: {data}"
            )
