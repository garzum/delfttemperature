class Weather:
    _current_temperature: float

    @property
    def current_temperature(self):
        return self._current_temperature

    @current_temperature.setter
    def current_temperature(self, temperature: str) -> None:
        self._current_temperature = float(temperature)
