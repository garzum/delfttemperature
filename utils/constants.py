# Dummy URL
# WEATHER_DATA_URL = "https://192.168.52.3"

# Working URL
WEATHER_DATA_URL = "https://weerindelft.nl/clientraw.txt"

# Timeout for the REST API request
REQUEST_TIMEOUT = 20

# Retry decorator arguments
RETRIES = 3
DELAY = 5
