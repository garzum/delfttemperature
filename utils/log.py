import logging


def setup_logger() -> logging.Logger:
    """Setups and returns logger

    Returns:
        logging.Logger: logger object
    """
    logger = logging.getLogger("HoeWarmIsHetInDelft")
    ch = logging.StreamHandler()
    ch.setLevel(logging.INFO)
    formatter = logging.Formatter("%(asctime)s: %(levelname)s: %(message)s")
    ch.setFormatter(formatter)
    logger.addHandler(ch)
    return logger
