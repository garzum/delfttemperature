import logging

import requests

from utils.constants import REQUEST_TIMEOUT

logger = logging.getLogger("HoeWarmIsHetInDelft")


class WeatherForecast:
    def request(url: str) -> str:
        """Sends a GET request to specified url and returns response

        Args:
            url (str): resource URL

        Returns:
            str: data received from the server
        """
        try:
            response = requests.get(url, timeout=REQUEST_TIMEOUT)
            response.raise_for_status()
            logger.debug(f"Successfully gathered weather data: {response.text}")
            return response.text
        except requests.exceptions.RequestException as e:
            logger.warning(f"Failed to retrieve data from the {url}: {e}")
            raise e
