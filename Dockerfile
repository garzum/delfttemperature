FROM python
WORKDIR /usr/src/app
COPY requirements.txt .
RUN pip install -r requirements.txt
COPY HoeWarmIsHetInDelft.py .
RUN mkdir utils
COPY utils/* utils/
ENTRYPOINT ["python", "HoeWarmIsHetInDelft.py"]
