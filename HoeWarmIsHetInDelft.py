# 1. Create a Python script called 'HoeWarmIsHetInDelft.py' that retrieves from http://www.weerindelft.nl/  the current temperature in Delft and prints it to standard output, rounded to degrees Celsius.
# Example expected output: 18 degrees Celsius

# 2. Write an appropriate dockerfile to containerize the script developed in point 1

# 3. Write a simple pipeline on https://www.gitlab.com that builds the container above and then executes it.

# We'll review the code based on clarity and correctness. It is important for the code to be robust, run correctly in a pipeline environment and to be easily troubleshootable by other DevOps engineers.


from retry import retry

from utils.constants import DELAY, RETRIES, WEATHER_DATA_URL
from utils.log import setup_logger
from utils.Weather import Weather
from utils.WeatherForecast import WeatherForecast
from utils.WeatherPropertiesParser import WeatherPropertiesParser


def main():
    @retry(tries=RETRIES, delay=DELAY)
    def get_and_parse_wather(weather: Weather):
        raw_data = WeatherForecast.request(WEATHER_DATA_URL)
        WeatherPropertiesParser.parse(raw_data, weather)

    logger = setup_logger()
    weather = Weather()
    try:
        get_and_parse_wather(weather)
        print(f"{round(weather.current_temperature)} degrees Celsius")
    except Exception as e:
        logger.error(
            f"Failed to get and parse weather data after {RETRIES} retries: {e}"
        )


if __name__ == "__main__":
    main()
